import React from 'react';
import logo from './logo.svg';
import './Styles/App.css';
import HolaMundo from './Components/Header'
import Main from './Components/Main'
import Aside from './Components/Aside'
import Header from './Components/Header';
import Nav from './Components/Nav'
import Footer from './Components/Footer';


function App() {
const mivariable = {
  headClass:"ClassHeader",
  head: "<HEADER>",
  navegar: "<NAV>"
}


  return (
    <div>

      <Header info={mivariable}></Header>
      <div className="container">
        <Nav info={mivariable}></Nav>
        <Main></Main>
        <Aside></Aside>
      </div>
      <Footer></Footer>

    </div>

  );
}

export default App;
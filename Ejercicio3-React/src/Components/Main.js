import React, { Component } from 'react';
import '../Styles/Main.css'

class Main extends Component {
    render() {

        return (
            <div className="main">
                <main ><p>Main</p></main>
                <div className="mod">
                    <aside className="uno">aside</aside>
                    <footer className="dos">footer</footer>
                </div>
            </div>
        );
    }
};

export default Main;
import React, { Component } from 'react';
import '../Styles/Header.css';

class Header extends Component {
    render() {


        return (
            <div className="head">
                <header className={(this.props.info.headClass)}>
                    <p>{this.props.info.head}</p>
                </header>
                <p></p>
            </div>
        );
    }
};
export default Header;